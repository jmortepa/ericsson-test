import parent


class Cousin(parent.Parent):
    def string_sum(self):
        sum_amount = self.sum

        if sum_amount is not None:
            return 'SUM: ' + str(self.value_a) + '+' + str(self.value_b) + ' is ' + str(sum_amount)
        else:
            return ''

    @property
    def outputdetails(self):
        output_dict = {'a': self.value_a,
                       'b': self.value_b,
                       'total': self.sum,
                       'output_string': self.string_sum(),
                       'error_output': [],
                       'errors_number': 0}

        if self.sum is None:
            output_dict['error_output'].append('Sum not allowed')
            output_dict['errors_number'] += 1

        if self.value_a is None:
            output_dict['error_output'].append('Value a not set')
            output_dict['errors_number'] += 1

        if self.value_b is None:
            output_dict['error_output'].append('Value b not set')
            output_dict['errors_number'] += 1

        return output_dict

    @property
    def value_a(self):
        return self._Parent__valuea

    @property
    def value_b(self):
        return self._Parent__valueb

    @value_a.setter
    def value_a(self, value):
        try:
            self._Parent__valuea = int(float(value))
        except:
            self._Parent__valuea = value

    @value_b.setter
    def value_b(self, value):
        try:
            self._Parent__valueb = int(float(value))
        except:
            self._Parent__valueb = value

    @property
    def sum(self):
        try:
            return int(self.value_a) + int(self.value_b)
        except:
            return None
