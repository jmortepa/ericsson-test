import argparse

import cousin

if __name__ == '__main__':
    parser = argparse.ArgumentParser('Process numbers.')
    parser.add_argument('numbers', nargs=2, help='Introduce two numbers separated with a blank space.')
    args = parser.parse_args()

    cousin_instance = cousin.Cousin()

    cousin_instance.value_a = args.numbers[0]
    cousin_instance.value_b = args.numbers[1]

    print(cousin_instance.as_json())
