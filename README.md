# Ericsson Test

The aim of this test is to develop a tool to provide a dictionary based on two input values.
The result should be given as follows

Example 1:
```
{
    "a": 5,
    "b": "hello",
    "total": null,
    "errors_number": 1,
    "error_output": ["Sum not allowed"], #for example if one of the parameters is a string
    "output_string": ""
}
```

Example 2:
````
{
    "a": 5,
    "b": 10,
    "total": 15,
    "errors_number": 0,
    "error_output": []
    "output_string": "SUM: 5+10 is 15"
}
````

The implementation includes the following components

* `Cousin` class heritage from given `Parent` class.
* Getters and setters for the given values of the sum.
* A read-only property of `Cousin` called `sum` which returns the sum of the arguments.
* A function of `Cousin` called `string_sum` which returns the string of the sum, `output_string`.
* Implementation of `outputdetails` in `Cousin`.

For testing purposes, a script with unittest, `test_cousin.py`, is provided. 12 different test cases
are considered.

Also a example script using `Cousin` class is included. It takes two arguments from the command line
and obtains the `outputdetails` property in JSON format. This example script needs to be executed as follows

```>> python example_script.py 1 2```

where `1` is the first sum value or `a` and `2` is the second sum value or `b`.

For parsing the arguments, `argparse` has been used, so it requires at least 2 input arguments.

## Implementation explanation

The following scripts are used to solve the problem

* `cousin.py`: Includes the `Cousin` class.
* `parent.py`: Includes the `Parent` class.
* `test_cousin.py`: Includes the test cases for the `Cousin` class.
* `example_script.py`: Includes the required example.

### Considerations

In the implementation has been considered that all numbers are integers, in case that a floating
number is introduced, it is parsed to integer format. 

In a practical example, this input

```>> python example_script.py 5.5 10.5```

will produce this output

````
{
    "a": 5,
    "b": 10,
    "total": 15,
    "errors_number": 0,
    "error_output": []
    "output_string": "SUM: 5+10 is 15"
}
````

This can be also found in the test cases.

The Python version used is `Python 3.6.1`.