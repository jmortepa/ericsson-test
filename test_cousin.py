import json
import unittest

import cousin


class TestCousin(unittest.TestCase):
    def test_no_setter(self):
        cousin_instance = cousin.Cousin()

        expected_dict = {'a': None,
                         'b': None,
                         'total': None,
                         'output_string': '',
                         'error_output': ['Sum not allowed', 'Value a not set', 'Value b not set'],
                         'errors_number': 3}

        expected_string = json.dumps(expected_dict, indent=4)

        self.assertDictEqual(cousin_instance.outputdetails, expected_dict)
        self.assertEqual(cousin_instance.as_json(), expected_string)

    def test_sum_access(self):
        cousin_instance = cousin.Cousin()

        with self.assertRaises(AttributeError):
            cousin_instance.sum = 1

    def test_string_sum(self):
        cousin_instance = cousin.Cousin()

        cousin_instance.value_a = '5'
        cousin_instance.value_b = '10'

        self.assertEqual(cousin_instance.string_sum(), 'SUM: 5+10 is 15')

    def test_string_sum_error(self):
        cousin_instance = cousin.Cousin()

        cousin_instance.value_a = 'a'
        cousin_instance.value_b = '10'

        self.assertEqual(cousin_instance.string_sum(), '')

    def test_sum_property(self):
        cousin_instance = cousin.Cousin()

        cousin_instance.value_a = '5'
        cousin_instance.value_b = '10'

        self.assertEqual(cousin_instance.sum, 15)

    def test_sum_property_error(self):
        cousin_instance = cousin.Cousin()

        cousin_instance.value_a = 'a'
        cousin_instance.value_b = 'b'

        self.assertIsNone(cousin_instance.sum)

    def test_floating_error_sum(self):
        cousin_instance = cousin.Cousin()

        cousin_instance.value_a = '5,5'
        cousin_instance.value_b = '10,5'

        expected_dict = {'a': '5,5',
                         'b': '10,5',
                         'total': None,
                         'output_string': '',
                         'error_output': ['Sum not allowed'],
                         'errors_number': 1}

        expected_string = json.dumps(expected_dict, indent=4)

        self.assertEqual(cousin_instance.as_json(), expected_string)

    def test_floating_sum(self):
        cousin_instance = cousin.Cousin()

        cousin_instance.value_a = '5.5'
        cousin_instance.value_b = '10.5'

        expected_dict = {'a': 5,
                         'b': 10,
                         'total': 15,
                         'output_string': 'SUM: 5+10 is 15',
                         'error_output': [],
                         'errors_number': 0}

        expected_string = json.dumps(expected_dict, indent=4)

        self.assertEqual(cousin_instance.as_json(), expected_string)

    def test_normal_sum(self):
        cousin_instance = cousin.Cousin()

        cousin_instance.value_a = '5'
        cousin_instance.value_b = '10'

        expected_dict = {'a': 5,
                         'b': 10,
                         'total': 15,
                         'output_string': 'SUM: 5+10 is 15',
                         'error_output': [],
                         'errors_number': 0}

        expected_string = json.dumps(expected_dict, indent=4)

        self.assertEqual(cousin_instance.as_json(), expected_string)

    def test_error_string_sum(self):
        cousin_instance = cousin.Cousin()

        cousin_instance.value_a = '5'
        cousin_instance.value_b = 'a'

        expected_dict = {'a': 5,
                         'b': 'a',
                         'total': None,
                         'output_string': '',
                         'error_output': ['Sum not allowed'],
                         'errors_number': 1}

        expected_string = json.dumps(expected_dict, indent=4)

        self.assertEqual(cousin_instance.as_json(), expected_string)

    def test_outputdetails_string(self):
        cousin_instance = cousin.Cousin()

        cousin_instance.value_a = '5'
        cousin_instance.value_b = 'a'

        expected_dict = {'a': 5,
                         'b': 'a',
                         'total': None,
                         'output_string': '',
                         'error_output': ['Sum not allowed'],
                         'errors_number': 1}

        self.assertDictEqual(cousin_instance.outputdetails, expected_dict)

    def test_outputdetails(self):
        cousin_instance = cousin.Cousin()

        cousin_instance.value_a = '5'
        cousin_instance.value_b = '10'

        expected_dict = {'a': 5,
                         'b': 10,
                         'total': 15,
                         'output_string': 'SUM: 5+10 is 15',
                         'error_output': [],
                         'errors_number': 0}

        self.assertDictEqual(cousin_instance.outputdetails, expected_dict)


if __name__ == '__main__':
    unittest.main()
