class Parent(object):
    def __init__(self):
        self.__valuea = None
        self.__valueb = None

    def string_sum(self):
        raise NotImplementedError()

    @property
    def outputdetails(self):
        raise NotImplementedError()

    def as_json(self):
        import json
        return json.dumps(self.outputdetails, indent=4)
